import '../styles/index.scss';
import { tarifs } from './constants';


// Variables

let balance = 100;
const payment = {};
const payments = [];
let currentPaySum = 0;


// Simulating selected companies

const selectCompany = document.querySelectorAll('.left__company');

for (let i = 0; i < selectCompany.length; i++) {
    selectCompany[i].addEventListener("click", function() {
        for (let z = 0; z < selectCompany.length; z++) {
            selectCompany[z].classList.remove('selected');
        }
        selectCompany[i].classList.add('selected');
        payment['id'] = this.dataset.id;
        document.getElementById('variableTitle').innerText = this.dataset.title;
        document.getElementById('variableDescript').innerText = this.dataset.description;
    });
}


// Meter logic

const valMeters = document.getElementById('meters');

function checkMeters() {
    payment['meterId'] = valMeters.options[valMeters.selectedIndex].text;
}

checkMeters();

valMeters.addEventListener('change', function() {
    checkMeters();
});


// Previous - Current meters logic

const previousInput = document.getElementById('previous');
const currentInput = document.getElementById('current');

function setInputFilter(objectId, textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {

        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }

        payment[objectId] = this.value;
        
       

        if (payment['previous'].length >= 1) { // First validate "Current" & "Previous"

            if ((payment['current'] - payment['previous']) <= 0) {
                currentInput.setAttribute('style', 'color: red;');
            } else {
                currentInput.removeAttribute('style');
            }

        }

      });
    });
  }

setInputFilter('previous', previousInput, function(value) {
    return /^\d*\.?\d*$/.test(value); 
});

setInputFilter('current', currentInput, function(value) {
    return /^\d*\.?\d*$/.test(value); 
});


// Save payment logic

const saveNewPayment = document.getElementById('save-payment');
const allPaymentList = document.getElementById('all-payment-list');

saveNewPayment.addEventListener("click", function(event) {

  event.preventDefault();
  const paymentsFind = localStorage.getItem('payments');
  
  if (Object.keys(payment).length < 4) {  // First save validation
    alert('Add all payment details!!!');
    throw new Error('Add all payment details!!!');
  }

  if ((payment['current'] - payment['previous']) <= 0) {  // Second save validation
    alert('Enter the correct meter data !!!');
    throw new Error('Enter the correct meter data !!!');
  }

  // Add result sum from tarif

  const currentTarif = payment['id'].toString();
  const useTarif = tarifs[currentTarif];

  payment['tarifResult'] = ((payment['current'] - payment['previous']) * (useTarif * 100)) / 100;

  // Continue saving

  if (paymentsFind === null ) {

      localStorage.setItem('payments', JSON.stringify([payment])); 

  } else {

      let paymentsArray = [];
      const parseValue = JSON.parse(paymentsFind);
      paymentsArray = [...parseValue];
      paymentsArray.push(payment);
      localStorage.setItem('payments', JSON.stringify(paymentsArray));
      
  }
  
  // Update fields values

  mainInfoUpdate();

});


// Update field value

function mainInfoUpdate() {

  for (let z = 0; z < selectCompany.length; z++) {
    selectCompany[z].classList.remove('selected');
  }
  displayPayments();
  previousInput.value = '';
  currentInput.value = '';
  findResultSum();
  updateRightBar();

}


// Update right bar 

function updateRightBar() { // Для сохраненных платежей добавляю чекбокс (доступно изменеие чекбокса тольок через сохранение платежей и отлату / ручное управление через клик по чекбоксу заблокировано)

  const currentSavedItems = localStorage.getItem('payments');
  const parseValueSavedItem = JSON.parse(currentSavedItems);
  const allRightFields = document.getElementsByClassName('right__payments-field');

  if (currentSavedItems !== null) {

    for (let i = 0; i < parseValueSavedItem.length; i++) {

      const currentSavedItem = parseValueSavedItem[i].id;

      for (let z = 0; z < allRightFields.length; z++) {

        const currentInput = allRightFields[z].querySelector('input');

        if(currentSavedItem == currentInput.getAttribute('data-id')) {
          currentInput.setAttribute('checked', 'checked');
        }

      }

    }

  } else {

    for (let i = 0; i < allRightFields.length; i++) {
      const currentInput = allRightFields[i].querySelector('input');
      currentInput.removeAttribute('checked');
    }

  }

}

updateRightBar();


// Display payments list

function displayPayments() {

  const refreshPayment = localStorage.getItem('payments');
  const checkForDeleting = document.querySelectorAll('.list-detected');

  checkForDeleting.forEach(el => el.remove());

  if (refreshPayment !== null ) {

    let displayPaymentsArr =[];
    const parseValue = JSON.parse(refreshPayment);
    displayPaymentsArr = [...parseValue];

    displayPaymentsArr.reduce(function (acc, obj) {  // Find final sum
      return currentPaySum = Number((acc + obj.tarifResult).toFixed(1));
    }, 0);

    for (let i = 0; i < displayPaymentsArr.length; i++) {

      // Find current price

      const meterResult = displayPaymentsArr[i].tarifResult;

      // Display element

      const liElementn = document.createElement('li');
      liElementn.setAttribute('class', 'list__item list-detected');
      liElementn.innerHTML = `<p><span class="list__item-label">${displayPaymentsArr[i].meterId}</span><span class="price">$ <b>${meterResult}</b></span></p>`;

      allPaymentList.insertBefore(liElementn, allPaymentList.firstChild);

    }

  }

}

displayPayments();


// Display payment Sum

function findResultSum() {

  const currentSumSpan = document.getElementById('currentSum');

  currentSumSpan.innerHTML = `$ <b>${currentPaySum}</b>`;

}

findResultSum();


// Clear logic 

const clearPayments = document.getElementById('clear-payment');

clearPayments.addEventListener("click", function(event) {

  event.preventDefault();
  localStorage.removeItem('payments');
  location.reload();

});


// Final Payment 

const finalPayment = document.getElementById('final-payment');
const finalPaymentList = document.getElementById('final-payment-list');

finalPayment.addEventListener("click", function(event) {

  balance = 100; // Обновил цену к изначальной для тестирования, без перезагрузки страницы для обновления денег на балансе

  event.preventDefault();
  const paymentsFind = localStorage.getItem('payments');
  const parsePaymentsFind = JSON.parse(paymentsFind);
  const allRightNameFields = document.getElementsByClassName('right__payments-field');
  const checkForDeletingList = document.querySelectorAll('.clear-item-reset');
  
  checkForDeletingList.forEach(el => el.remove()); // Clear success & other payments from list 

  for (let i = 0; i < parsePaymentsFind.length; i++) {  // Sorting out payments

    let paymentName = '';
    const currentPrice = parsePaymentsFind[i].tarifResult
    balance = balance - currentPrice;

    for (let y = 0; y < allRightNameFields.length; y++) { // Find payment name

      const currentInput = allRightNameFields[y].querySelector('input');
      if(parsePaymentsFind[i].id == currentInput.getAttribute('data-id')) {
        paymentName = currentInput.getAttribute('data-name');
      }

    }

    if (balance >= 0) { // Check balance

      const liElementn = document.createElement('li');
      liElementn.setAttribute('class', 'list__item clear-item-reset');
      liElementn.innerHTML = `${paymentName}: успешно оплачено`;

      finalPaymentList.append(liElementn);
      console.log(`${paymentName} - оплачено`);

    } else { // balance is over

      const liElementn = document.createElement('li');
      liElementn.setAttribute('class', 'list__item clear-item-reset list__item-error');
      liElementn.innerHTML = `${paymentName}: ошибка транзакции`;

      finalPaymentList.append(liElementn);
      throw new Error(`${paymentName} - ошибка транзакции`);

    }
  
  }

  localStorage.removeItem('payments');
  currentPaySum = 0;
  mainInfoUpdate();

});

// Не большое уточнение:
// *Так как в условии не было информации о том, можно ли добавлять платежы одного типа, то я сдела так , чтобы к примеру мы могли добавить нескольок квитанций за интернет(вдруг кто-то за 2 месяца оплачивает) и при оплате у нас выведеть тогда под каждый платеж отдельное сообщение об успешной или не успешной транзакции
